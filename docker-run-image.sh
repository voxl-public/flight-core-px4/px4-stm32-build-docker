#!/bin/bash
IMAGE_NAME=px4-stm32-build
IMAGE_TAG=0.0

docker run --rm -it --privileged \
    --mount type=bind,source="$(pwd)",target=/home/user \
    "${IMAGE_NAME}":"${IMAGE_TAG}" /bin/bash
