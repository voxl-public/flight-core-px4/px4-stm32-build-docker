# px4-stm32-build-docker

Docker Project for building the PX4 for STM32 based SoCs

## Summary

This project is used to generate a docker image that can then be used as a build host.  This means you don't have to deal with setting up tools, which can be a great deal of pain!

It does mean you need to have a bare minimum knowledge of git, bash and Docker.

## Requirements

- Git
- Docker

## To Build Image

Use helper script:

```
./docker-build-image.sh
```

## To Use Image

Copy the `docker-run-image.sh` script to the root of the bootloader project and run it (or inspect the contents of the script and run yourself).
